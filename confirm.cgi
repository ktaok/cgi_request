#! /usr/local/bin/perl



require '../../lib/init.cgi';



$ga     = include("../../html/ssi/ga.html");
$header = include("../../html/ssi/header.html");
#$footer = include("../../html/ssi/footer.html");
# クッキー情報を取得
$cookie = $ENV{'HTTP_COOKIE'};

# 取得したクッキー情報を分解
@pairs = split(/;/, $cookie);
foreach $pair (@pairs) {
	local($name,$value) = split(/=/, $pair);
	$name =~ s/\s//g;
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	if($name eq 'id'){
		$COOKIE{$name} = $value;
	}
}
if($COOKIE{id}){
	$footer        = include("../../html/ssi/footer_member.html");
}else{
	$footer        = include("../../html/ssi/footer.html");
}



if($ENV{'REQUEST_METHOD'} eq "POST"){
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
}

@pairs = split(/&/,$buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$value =~ s/&/&amp;/g;		# &を変換
	$value =~ s/"/&quot;/g;		# "を変換
	$value =~ s/</&lt;/g;		# <を変換
	$value =~ s/>/&gt;/g;		# >を変換
	$value =~ s/\r\n/\n/g;		# DOS/Windowsでの改行コード(\r\n)をPerlの改行コード(\n)に変換
	$value =~ s/\r/\n/g;		# MacOSでの改行コードを(\r)をPerlの改行コード(\n)に変換
	chomp $value;			# 末尾の改行コードを削除
	$value =~ s/\n/<br \/>/g;		# Perlの改行コード(\n)をHTMLの改行文字(<BR>)に変換
	$value =~ s/	//g;		# ログの区切り文字を削除
	$FORM{$name} = $value;
}



# パーソナル情報の取得
%personal=&personal;



# 入力されたデータのチェック
# 年代
if(!$FORM{'age'}){$error .= '年代が選択されていません<br>';}
# 性別
if(!$FORM{'sex'}){$error .= '性別が選択されていません<br>';}
# ご意見・ご要望
if(!$FORM{'comment'}){$error .= 'ご意見・ご要望が入力されていません<br>';}

if($error){&error($error);}



$age = $FORM{'age'};
$sex = $FORM{'sex'};
$comment = $FORM{'comment'};



$FORM{'comment'} =~ s/<br \/>/\n/ig;



# 表示開始
print "Content-type: text/html\n\n";
print <<EOF;
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>リクエスト｜$site_name</title>
<meta name="keywords" content="リクエスト">
<meta name="description" content="$site_name リクエスト">
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="/css/import.css" type="text/css" media="all">
<link rel="stylesheet" href="/css/request.css" type="text/css" media="all">
<link href="/img/favicon_iphone.png" rel="apple-touch-icon" />
<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />
<link href="/img/favicon.png" rel="icon" type="image/png" />
$ga
</head>
<body id="request">

$header

<!-- CONTENT -->
<article>
<section id="confirm">
<h2>リクエスト</h2>
<h3>リクエスト入力内容確認</h3>
<p>以下の内容でよろしければ「送信する」ボタンを押して下さい。</p>
<form action="regist.cgi" method="post">
<input type="hidden" name="age" value="$FORM{'age'}">
<input type="hidden" name="sex" value="$FORM{'sex'}">
<input type="hidden" name="comment" value="$FORM{'comment'}">
<fieldset>
<legend>リクエストフォーム</legend>
<dl>
<dt>年代</dt>
<dd>$age</dd>
<dt>性別</dt>
<dd>$sex</dd>
<dt></dt>
<dd>$comment</dd>
</dl>
</fieldset>
<ul>
<li><input type="button" value="戻る" onclick="history.back()" class="btn"></li>
<li><input type="submit" value="送信する" class="btn"></li>
</ul>
</form>
</section>
</article>
<!-- //CONTENT -->

$footer

</body>
</html>
EOF



;############################# エラー関数 ##############################
sub error {
	$error = $_[0];

# 表示開始
print "Content-type: text/html\n\n";
print <<EOF;
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>エラー｜$site_name</title>
<meta name="keywords" content="エラー">
<meta name="description" content="エラー">
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="/css/import.css" type="text/css" media="all">
<link rel="stylesheet" href="/css/request.css" type="text/css" media="all">
<link href="/img/favicon_iphone.png" rel="apple-touch-icon" />
<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />
<link href="/img/favicon.png" rel="icon" type="image/png" />
$ga
</head>
<body id="request">

$header

<!-- CONTENT -->
<article>
<section id="error">
<h2>リクエスト</h2>
<h3>エラー</h3>
<p>$error</p>
<a class="back" onClick="history.back()">戻る</a>
</section>
</article>
<!-- //CONTENT -->

$footer

</body>
</html>
EOF
	exit;
}


