#! /usr/local/bin/perl



require '../../lib/init.cgi';



$ga     = include("../../html/ssi/ga.html");
$header = include("../../html/ssi/header.html");
#$footer = include("../../html/ssi/footer.html");
# クッキー情報を取得
$cookie = $ENV{'HTTP_COOKIE'};

# 取得したクッキー情報を分解
@pairs = split(/;/, $cookie);
foreach $pair (@pairs) {
	local($name,$value) = split(/=/, $pair);
	$name =~ s/\s//g;
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	if($name eq 'id'){
		$COOKIE{$name} = $value;
	}
}
if($COOKIE{id}){
	$footer        = include("../../html/ssi/footer_member.html");
}else{
	$footer        = include("../../html/ssi/footer.html");
}


# ログファイル
$log_file = '../edit/request_log/request.log';

# ログディレクトリ
$log_dir = '../edit/request_log/data';



if($ENV{'REQUEST_METHOD'} eq "POST"){
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
}

@pairs = split(/&/,$buffer);
foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
#	$value =~ s/&/&amp;/g;		# &を変換
#	$value =~ s/"/&quot;/g;	# "を変換
#	$value =~ s/</&lt;/g;	# <を変換
#	$value =~ s/>/&gt;/g;	# <を変換
	$value =~ s/\r\n/\n/g;		# DOS/Windowsでの改行コード(\r\n)をPerlの改行コード(\n)に変換
	$value =~ s/\r/\n/g;		# MacOSでの改行コードを(\r)をPerlの改行コード(\n)に変換
	chomp $value;			# 末尾の改行コードを削除
	$value =~ s/\n/<br \/>/g;		# Perlの改行コード(\n)をHTMLの改行文字(<BR>)に変換
	$value =~ s/	//g;		# ログの区切り文字を削除
	$FORM{$name} = $value;
}



# パーソナル情報の取得
%personal=&personal;



# 二重投稿防止
open(DB,"$log_file");
@lines = <DB>;
close(DB);
foreach $line (@lines){
	($id, $flag, $time, $date, $personal_id, $personal_name, $age, $sex, $comment, $agent, $ip) = split(/\t/, $line);
	if($FORM{'comment'} eq $comment){ goto "DENY"; }
}



# 入力されたデータのチェック
if(!$FORM{'comment'}){$error .= 'コメントが入力されていません<br>';}
if($error){&error($error);}



# ファイルロック
$tm = 50;
while(TRUE){
	if(symlink("$log_file", "$log_file.lock")){
		last;
	}else{
		sleep(1);
		$tm--;
		if($tm <= 0){
			&error('システムエラーです(-3)');
		}
	}
}



# $idの作成
# 現在時刻
($sec, $min, $hour, $mday, $mon, $year, $wday) = localtime();
$year += 1900;
$mon++;
$id = sprintf("%04d%02d%02d%02d%02d%05d", $year, $mon, $mday, $hour, $min, $$);



# 書き込み用レコードの作成

# 投稿日時
$time_now = time();
($sec, $min, $hour, $mday, $mon, $year, $wday) = localtime();
$year += 1900;
$mon++;
$date_now = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year, $mon, $mday, $hour, $min, $sec);

$value = "$id	reserve	$time_now	$date_now	$personal{'id'}	$personal{'name'}	$FORM{'age'}	$FORM{'sex'}	$FORM{'comment'}	$ENV{'HTTP_USER_AGENT'}	$ENV{'REMOTE_ADDR'}\n";



open(DB,">>$log_dir/$id.log");
print DB $value;
close(DB);
chmod(0666, "$log_dir/$id.log");

opendir(DB, "$log_dir");
@filelist = readdir(DB);
closedir(DB);

@lines2 = '';
foreach $file (@filelist) {
	if($file eq "."){next;}
	if($file eq ".."){next;}

	open(DB,"$log_dir/$file");
	$line = <DB>;
	close(DB);

	push(@lines2,$line);
}

# データをソート
@lines = map {$_->[0]}
	sort {$b->[1] <=> $a->[1]}
		map {[$_, split /\t/]} @lines2;

open(DB,">$log_file");
print DB @lines;
close(DB);



# ロック解除
unlink("$log_file.lock");



DENY:



# 表示開始
print "Content-type: text/html\n\n";
print<<EOF;
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>リクエスト｜$site_name</title>
<meta name="keywords" content="リクエスト">
<meta name="description" content="$site_name リクエスト">
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="/css/import.css" type="text/css" media="all">
<link rel="stylesheet" href="/css/request.css" type="text/css" media="all">
<link href="/img/favicon_iphone.png" rel="apple-touch-icon" />
<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />
<link href="/img/favicon.png" rel="icon" type="image/png" />
$ga
</head>
<body id="request">

$header

<!-- CONTENT -->
<article>
<section id="regist">
<h2>リクエスト</h2>
<h3>リクエスト送信完了</h3>
<p>送信完了しました。<br>
ありがとうございました。</p>
<a href="/" class="back">HOMEへ戻る</a>
</section>
</article>
<!-- //CONTENT -->

$footer

</body>
</html>
EOF


;############################# エラー関数 ##############################
sub error {
	$error = $_[0];

# 表示開始
print "Content-type: text/html\n\n";
print <<EOF;
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>エラー｜$site_name</title>
<meta name="keywords" content="エラー">
<meta name="description" content="エラー">
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="/css/import.css" type="text/css" media="all">
<link rel="stylesheet" href="/css/request.css" type="text/css" media="all">
<link href="/img/favicon_iphone.png" rel="apple-touch-icon" />
<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />
<link href="/img/favicon.png" rel="icon" type="image/png" />
$ga
</head>
<body id="request">

$header

<!-- CONTENT -->
<article>
<section id="error">
<h2>リクエスト</h2>
<h3>エラー</h3>
<p>$error <a class="back" onclick="history.back()">戻る</a></p>
</section>
</article>
<!-- //CONTENT -->

$footer

</body>
</html>
EOF
	exit;
}


